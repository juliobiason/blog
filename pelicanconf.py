#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# pylint:disable=missing-docstring

from __future__ import unicode_literals
import os

AUTHOR = u'Julio Biason'
SITENAME = u'JulioBiason.Net 3.0'
SITEURL = 'http://blog.juliobiason.net'
SITESUBTITLE = u'Old-school coder living in a 2.0 development world.'
SINGLE_AUTHOR = True
DEFAULT_DATE_FORMAT = '%Y-%m-%d'
DEFAULT_CATEGORY = 'random'
MINT = False
BASE_PATH = os.path.dirname(os.path.realpath(__file__))
PATH = os.path.join(BASE_PATH, 'content')

TIMEZONE = 'America/Sao_Paulo'

DEFAULT_LANG = u'en'

ARTICLE_URL = '{category}/{date:%Y}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{date:%Y}/{slug}.html'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS = [
    ('Presentations', 'http://presentations.juliobiason.net/')
]

# Social widget
SOCIAL = (
    ('twitter-square', 'https://twitter.com/juliobiason'),
    ('file-text', 'http://presentations.juliobiason.net/'),
    ('github', 'https://github.com/jbiason'),
    ('bitbucket', 'https://bitbucket.org/juliobiason'),
    ('book', 'https://www.gitbook.com/@juliobiason'),
    ('reddit-square', 'http://www.reddit.com/user/juliob'),
    ('facebook-official', 'https://www.facebook.com/julio.biason'),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# THEME = './pelican-chunk'
# THEME = './pelican-bootstrap3'
# THEME = './BootHack'
# THEME = './pelican-fancy-foundation'
THEME = os.path.join(BASE_PATH, 'pure-single')

USE_FOLDER_AS_CATEGORY = True

# related to the theme
# SITELOGO = 'images/1500x500.png'
SITE_LOGOS = {'default': 'images/1500x500.png'}     # pelican-fancy-foundation
COVER_IMG_URL = '/images/1500x500.png'               # pure-single
PROFILE_IMG_URL = '/images/profile.jpg'                 # pure-single
TAGLINE = 'Old school coder living in a 2.0 dev world'  # pure-single
GOOGLE_ANALYTICS = 'UA-2055225-5'                       # pure-single
