Example
#######

:date: 2010-10-03 10:20
:modified: 2010-10-04 18:40
:tags: thats, awesome
:slug: example
:authors: Alexis Metaireau, Conan Doyle
:summary: Short version for index and feeds
:status: draft


This is an example post. [#foot]_

.. code-block:: python

   # This is code
   print('Hellow world')


.. [#foot] A footnote description.
