Símbolos para o Desenvolvedor Python
####################################

:date: 2018-05-30 11:04
:modified: 2018-05-30 11:04
:tags: python, lisp, symbols, code
:slug: symbols-for-the-python-developer
:lang: pt
:summary: Lisp tem "Símbolos", que são indicadores de algo. Python não tem,
          mas a funcionalidade pode ser replicada.

Nas minhas breves excursões em Lisp, uma coisa que acabei gostando foram os
"símbolos".

Símbolos são elementos no código que não possuem nenhum valor, mas "existem".
Por exemplo, nesse código em Clojure:

.. code-block:: clojure

   (apply map vector [[:a :b :c]
                      [:d :e :f]
                      [:g :h :i]])

Esse é o exemplo na documentção de "map". Ele cria uma lista de listas, com
tres símbolos: ``:a``, ``:b``, ``:c`` e assim por diante. Esses símbolos não
tem valor algum, eles apenas *são*.

Mas o que isso tem a ver com Python?

Eu lembrei de símbolos recentemente, numa dicussão sobre o nosso código. Nós
tinhamos algo assim em um de nossos módulos:

.. code-block:: python

   ACCELERATION = 'acceleration'
   ACCESS_DENIED = 'access_denied'
   DELIVERY = 'delivery'
   REDIRECT = 'redirect'

Quem trabalha com Django deve ter visto vários exemplos como esse: Nós
normalmente criamos essas constantes e as colocamos em ``choices``. E aí é que
ocorreu a discussão: "Por que temos essas constantes se o nome é exatamente
igual às strings?"

Minha primeira reação foi reponder "para ajudar a garantir que o código está
correto, porque o uso de uma variável indefinida vai explodir, enquanto que
uma string 'indefinida' não". Mas a dicussão continuou, e aí a ideia de
símbolos reapareceu.

As variáveis acima define o valor armazenado no banco de dados. Se o usuário
escolher a opção ``ACCELERATION``, ``acceleration`` será guardada no banco de
dados. Mas o valor é importante? Se mudássemos ``ACCELERATION =
'acceleration'`` para ``ACCELERATION = 'oba-oba'``, isso iria fazer qualquer
diferença no código? A resposta é "não", porque ao invés de verificar se o
valor é o mesmo ``acceleration``, iria comparar com ``oba-oba`` e ainda
funcionar.

(Obviamente, você deve estar comparando sempre com a constante quando estiver
falando de "acceleration", não o valor da constante.)

E aí é que está o significado de Símbolos em Python: Se o valor em si não é
importante, de uma forma que você possa criar uma constante, mudar seu valor e
as coisas ainda funcionam (sem considerar entradas antigas, no caso), então
você tem um Símbolo.
