Everything Is an API
####################

:date: 2016-01-18 14:20
:tags: api, services, code, languages, database
:lang: en
:status: draft

So I've mentioned a bit of API on my `post about REST and Couchbase
<{filename}/code/couchbase-example-and-rest.rst>`_ but there is something that
I've always though and should share:

> Everything is an API.

That's something I've been carrying with me for a long time but hadn't any
idea how to start. Now I know.

What's an "API", anyway?
------------------------

I guess the first step into explaining what I mean by "Everything is an API"
is going into what an API is.

Wikipedia `defines API
<https://en.wikipedia.org/wiki/Application_programming_interface>`_ as "an
application programming interface (API) is a set of routines, protocols, and
tools for building software and applications". Now, this is terse and the
following paragraphs do a better job explaining what it is but in a very basic
term, an API is what people can use to connect things. You can connect your
project with mine through an API; your application will talk to the system via
an API; and so on.

The very important thing to keep in mind is that there is an "I" in "API" and
it stands for "interface". It's way easier to understand what an interface is:
it's the thing that you plug the other part and everything works fine. It's
also a way to `hide information
<https://en.wikipedia.org/wiki/Information_hiding>`_, so people have no idea
what's going being the curtain.

The "I" is for Hiding
---------------------

The last point, "informatino hiding", is the most important thing in an API.
When I was talking about the Couchbase example, I didn't explicity put the
flight paths in the API; you had to access it through the flights, where it
would list all the airports it go through, or through the airport, where it
would list all the flights that go through it. Nowhere you see that the
"flightpaths" collection [#collection]_ because it's deeply connected to the
way people will access it.

True, accessing it through the airports endpoint or through the flights
endpoint would end accessing the same results (with different views), but
whoever is accessing the external API don't need to know about it.

Your External API is an API
---------------------------

This is the most obvious thing I could've mentioned: As the Couchbase example,
the API reflects the actions done upon it: If I'm accessing an airport, I may
want to get all the flights that go through it; if I'm accessing a flight, I
may see all the airports that that flight go through. I may not need them
straight away, so maybe it's not a good idea to return the all the flights
that go through an airport when I get its base data -- hence, another reason
for it to be a sub-resource in the previous example.

If I could make a suggestion, let me say this: Create APIs based on *how*
you'd want to access an information from your system, **not** how the
underlaying system is build, no matter how much the `Conway's Law
<https://en.wikipedia.org/wiki/Conway's_law>`_ may compel you to do so. People
don't need to know how your company communicate, they need to know how your
data *flow*.

Your UI is an API
-----------------

"Now you're saying bollocks", you must be thinking. Ok, so answer me this:
What's the difference between clicking a button and calling
``$('#button').click()``. Ok, the first one is manual and the second could be
automated, but after that... is there any difference?

Also, isn't the UI an API for humans? I mean, instead of computers calling
this and that, it's people who is doing this and that.

So, as with external APIs, your UI should *not* reflect your company way of
communicating or the database behind the interface or anything else, it must
reflect the way the *data* flows through the system: Airports need to exist
before you can put flights on them; you need to create flights and *then*
associate them with airports.

.. [#collection] To use an expression from MongoDB.
