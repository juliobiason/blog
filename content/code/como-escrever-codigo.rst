Guia Randômico de Como Escrever Código
######################################

:date: 2017-10-16 10:12
:status: draft
:tags: programação, clean code
:summary: Um punhado de dicas de como escrever código.

Honestamente, esse deve ser o pior título de um post que eu já escrevi.
Infelizmente, eu não consegui em pensar num título melhor, porque o que eu vou
comentar são técnicas gerais de como se escreve código, desde código limpo até
código reusável.

Solucione o problema de agora
-----------------------------

Um dos graves problemas que programadores cometem é a tentativa de solucionar
problemas além do atual. Caso mais clássico nesses casos é a famigerada frase
"A gente pode precisar disso um dia".

O problema aqui é que ninguém sabe quando esse dia vai chegar. Ou, pior, se
vai chegar. Aí criam-se estruturas monstruosas numa tentativa de resolver
todos esses problemas -- problemas que não existem ainda -- e cria-se uma
aberração cuja resolução de qualquer problema mais simples passa por várias
camadas e várias alterações porque "um dia" um grande problema pode surgir --
e na maior parte das vezes, esse problema nunca aparece.

Relacionado: Ralph Johnson uma vez disse "Before software can be reusable it
first has to be usable." Ou seja, primeiro você faz o código ser usável
(solucionando o problema que você conhece), depois você faz o código ser
re-usável.

Pense em abstrações
-------------------

Um dos problemas que eu vejo muito é que algumas vezes programadores focam
demais no problema de **agora**, mas esquecem que pode haver uma explicação
mais óbvia para o problema de agora.

Por exemplo, se você estiver escrevendo um sistema de vendas, pode acontecer
de um determinado produto que não deve ser contabilizado. Algumas pessoas já
iriam pular para o "deixa eu colocar o código do produto aqui pra que a função
de contabilizar pule ele". Até funciona e soluciona o problema de agora, mas
faltou uma pequena parte "Por que?". Porque exatamente esse produto não
precisa ser contabilizado? É porque é brinde? Será que não seria melhor
adicionar um campo booleano "brinde" no banco de dados e validar isso?

É óbvio que sim.

Abstraia, mas com moderação
---------------------------

Quem estava prestando atenção deve estar se perguntando se a questão de
abstrair do ponto anterior não conflita com o primeiro ponto de solucionar o
problema de agora. E com razão.

Você pode começar a abstrair um problema até chegar num "visitor pattern" e
pensar "Mais isso é ótimo, agora estou preparado para o futuro". A resposta de
onde está o problema desse pensamento está justamente no final da frase: *você
nunca sabe o que vai acontecer no futuro, portanto pare de tentar resolver.*
Se o visitor soluciona seu problema **de agora**, então perfeito, mas não
continue subindo o nível de abstração para solucionar algo do futuro.

Solucione o problema por passos
-------------------------------

Um algoritmo é uma lista de passos que tem devem ser executados para se obter
algum resultado. Você trabalha todo dia, deve comparecer ao local de trabalho
nos horários indicados, deve concluir o máximo de tarefas que lhe são passadas
e, com isso, obtem-se o resultado do salário.

Uma aplicação não é tão diferente: O usuário seleciona um produto mas só pode
adquirir o mesmo se passar por uma série de validações:

- O produto está disponível no momento
- O cliente não tem saldo devedor
- Não existe uma restrição de compra desse cliente

E assim por diante. Cada uma dessas validações nada mais é do que um passo na
compra de um produto. E cada um desses passos deveria, em teoria, se torna uma
função.

A muito tempo atrás, eu li essa dica: "Comece sua aplicação com comentários,
descrevendo cada um dos passos que ela deve fazer. Agora escreva a código que
faz com que o passo/comentário seja executado". Logo na sequencia outra grande
pérola: "Converta cada um dos comentários em uma função, removendo o
comentário." É assim que código "auto-documentado" nasce: Ele descreve
exatamente o que é esperado, refletindo o que tem que acontecer do lado de
fora.

Por exemplo, se eu fosse escrever um aplicativo que transfere meus posts do
Mastodon para o Evernote, eu começaria da seguinte forma:

.. code-block:: python

   # conectar no evernote
   # conectar no mastodon
   # buscar informação de qual último post do mastodon foi encontrado
   # buscar posts no mastodon a partir do último post
   # para cada post encontrado, criar uma nova entrada no evernote
   
(E sim, eu sou desleixado e não desconecto dos serviços)

Agora eu tenho o layout básico da aplicação. E agora qualquer um que for olhar
o código fonte sabe exatamente o que ela faz.

As regras de negócio devem estar explícitas e juntas
----------------------------------------------------

Um dos problemas de tentar resolver problemas do futuro e/ou abstrair demais
e/ou não descrever os passos da aplicação é que coisas acabam caindo nos
lugares errados e acabam escondidas.

Por exemplo, no caso do exemplo do Mastodon-Evernote acima, alguém pode acabar
colocando a função que cria os posts no evernote dentro da função que busca os
posts -- afinal de contas, já que eu já tô com o post na mão, porque eu
simplesmente não mando pra frente?

A resposta é: Porque você vai esconder um dos passos importantes da aplicação
e quem for ler o código não vai ver exatamente *tudo* que a aplicação faz.

Solucione um problema por vez
-----------------------------

Muitas vezes estamos corrigindo alguma coisa e, ao ver outro problema,
pensamos: "Vou aproveitar que estou aqui mesmo e vou arrumar isso também".

Essa é uma das melhores formas de deixar as coisas pela metade.

Quando estiver resolvendo um problema (ou implementando uma nova feature)
foque em *terminar o que está fazendo* antes de tentar resolver outro problema
(ou adicionar outra feature).

Outro motivo pra começar algo e terminar esse algo antes de lidar com outra
coisa é aproveitar ao máximo os sistemas de controle de versão (como git e
SubVersion). Resolvendo um problema e adicionando as alterações no controle de
versão vai deixar o histórico de sistema correto, apontando exatamente o que
foi resolvido. Se você precisar desfazer alguma coisa, você garante que vai
desfazer só o que precisa ser realmente defeito, não aquilo que precisa ser
desfeito e mais aquela coisa que você aproveitou e corrigiu junto.

Esqueumorfismo vs realidade
---------------------------

Esqueumorfismo é uma palavra feia (pelo menos, recentemente). Esqueumorfismo é
a utilização de elementos reais em elementos virtuais (por exemplo, um
aplicativo de bloco de notas que tem o gráfico de uma espiral e linha para
parecer que o mesmo é um bloco de notas real).

Mas código está tentando resolver um problema real e, portanto, deve seguir os
passos e utilizar os nomes da vida real, por mais absurdos que pareçam.

Por exemplo, quando eu estava vendo contabilidade na aula de Técnicas
Comerciais (por favor, não procurem quando essa cadeira deixou de fazer parte
do currículo), havia a ideia de lançar dinheiro que entrava como débito e
dinheiro que saia como crédito. O que não faz o menor sentido pra qualquer um
que tentou cuidar da sua conta corrente. Mas se fossemos criar uma aplicação
para esse controle contábil, teríamos que fazer as entradas exatamente dessa
forma.

Represente a vida, mesmo que ela não faça sentido.

Não tenha medo de refatorar
---------------------------

Ainda com o problema acima, já que foi solucionado apenas um problema e outros
problemas podem surgir, o escopo da solução vai mudar. E por isso você não
precisa ter medo em transformar aquela classe e/ou função em outra coisa --
reduzir a classe a uma função utilitária ou mover a função para uma classe ou
módulo utilitário.

Obviamente, para isso você precisa ter certeza que as coisas vão funcionar e
pra isso você precisa de...

Teste a sua aplicação
---------------------

Uma questão que eu tenho levantado a bastante tempo é que testes devem testar
comportamentos e não implementações; você testa se o nome de um usuário é
válido, não se a função ``validate_name`` valida nome. Se amanhã, devido a
refatoração que você fez, que moveu a função de um lugar pra outro e a função
``validate_name`` sumiu por não ser mais necessária (porque agora as regras
batem com outra ou outra validação pode ser abstraída para lidar com vários
casos), nomes inválidos não devem continuar sendo bloqueados. É por isso que
devem ser escritos testes que descrevem como o sistema opera, não sobre as
funções.
