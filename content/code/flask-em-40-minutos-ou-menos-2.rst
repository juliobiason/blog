Flask em 40 Minutos ou Menos: A Aplicação
#########################################

:date: 2017-10-09 09:30
:status: draft
:slug: flask-em-40-minutos-ou-menos-2
:tags: python, web, flask
:lang: pt
:summary: Agora que temos o básico de como iniciar uma aplicação em Flask,
          vamos criar uma aplicação de fato.

Como `vimos anteriormente <{filename}/code/flask-em-40-minutos-ou-menos-1.rst>`_,
para começar uma aplicação Flask é preciso ter um VirtualEnv configurado com o
Flask instalado. Agora vamos criar uma aplicação "real" [#real]_ para depois
colocarmos em produção.

A aplicação que iremos criar é um sistema para mostrar atas de reuniões. As
reuniões serão registradas como arquivos Markdown e iremos apresentar as
mesmas no site.

Obviamente, essa é uma aplicação extremamente simples, mas ela cobre um bocado
de coisas: desde onde deixar os arquivos de origem, passando por arquivos de
template e arquivos estáticos.

Requisitos
----------

Como vimos, nosso primeiro requisito é o próprio Flask. Mas vamos precisar de
outro requisito: Algo que consiga converter arquivos Markdown para HTML, para
serem apresentados na web. E esse requisito é a biblioteca "Markdown". Como
queremos que o ambiente seja reproduzível, vamos adicionar essa dependência no
arquivo ``requirements.txt``:

.. code-block:: text

   flask~=0.12.2
   markdown~=2.6.9

(Você pode rodar ``python -m pip install -r requirements.txt`` novamente para
instalar essa biblioteca no virtualenv.)

A estrutura da aplicação
------------------------

Antes de começar a codificar a aplicação, vamos montar a estrutura de
diretórios para os dados que precisamos:

- O diretório base da aplicação vai conter, inicialmente, o nosso arquivo
  ``requirements.txt``;
- Dentro do diretório base, criamos um diretório com o nome do módulo/projeto.
  Como nosso projeto é um projeto de Atas, vamos chamar simplesmente de
  ``ata``;
- Como Python só consegue ver o conteúdo de diretórios quando há um arquivo
  chamado ``__init__.py`` dentro deste, temos que ter esse arquivo em ``ata``
  [#dirsasmodule]_ ;
- Dentro do diretório do módulo ``ata``, iremos criar os diretórios ``static``
  para conteúdo estático (como imagens, javascript e CSS) e ``templates`` para
  os templates utilizados pelo Flask;
- Um diretório chamado ``contents`` com os arquivos de atas que iremos
  utilizar como exemplo para os testes [#tests]_.

Uma vez que a estrutura de diretórios esteja criada, iremos ter os seguintes
arquivos (o conteúdo desses serão explicados a seguir, mas só para termos uma
ideia de como o projeto ficará no final):

- Dentro de ``ata`` teremos o arquivo ``__init__.py`` para o Python encontrar
  o mesmo, ``defaults.py`` com as configurações padrão do nosso aplicativo e
  ``main.py``, que terá o aplicativo Flask;
- Em ``ata/static``, teremos um arquivo ``style.css`` com o estilo aplicado ao
  nosso sistema;
- ``ata/templates`` terá dois arquivos: ``layout.html`` com a estrutura base
  das páginas e ``index.html`` com o template a ser apresentado no índice do
  sistema;
- E em ``contents`` teremos um arquivo markdown qualquer (com não definimos um
  formato para as atas, qualquer conteúdo Markdown será apresentado).

Então a estrutura de arquivos será a seguinte:

.. code-block:: text

   .
   |-- ata
   |   |-- __init__.py
   |   |-- defaults.py
   |   |-- main.py
   |   |-- static
   |   |   `-- style.css
   |   `-- templates
   |       |-- index.html
   |       `-- layout.html
   |-- contents
   |   `-- 20171031.md
   `-- requirements.txt

.. [#real] Para diferentes valores de "real".

.. [#dirsasmodule] Na verdade, a existência do ``__init__.py`` torna o
   diretório um módulo, e funções dentro do ``__init__.py`` poderiam ser
   importadas usando-se apenas ``import <diretório>``, mas não é o que vamos
   fazer aqui.

.. [#tests] Como essa é uma aplicação de exemplo, não irei entrar em como
   escrever testes para Flask, mas uma aplicação real de verdade, testes
   deveriam existir e seriam armazenados num diretório ``tests`` ao lado de
   ``ata``.
