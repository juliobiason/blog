Uma Não-Gentil Introdução do Stream Processing
##############################################

:date: 2018-10-29 11:28
:slug: intro-stream-processing
:summary: Uma breve explicação sobre stream processing, envolvendo
          processamento distribuído, programação funcional.
:tags: stream processing, flink, spark, batch processing, functional, pure
       functions
:status: draft

A empresa que eu trabalho é uma `CDN`_ com mais de 72 servidores espalhados
pelo mundo -- a grande maioria no Brasil -- e nós apresentamos o tráfego em
tempo real para nossos clientes, utilizando *stream processing*.

Mas para explicar stream processing, eu vou explicar todo o processo da
geração da informação de tráfego até chegar no nosso processador.

O Que É Uma CDN
===============

Antes de explicar o processamento dos dados, precisamos entender o que é uma
CDN.

Uma CDN é um cache -- simplificando a explicação enormemente.

Imagine que você é cliente de uma operadora e quer acessar um site de
e-commerce, que está na nossa CDN. Numa conexão normal, o seu computador irá
se conectar com o seu modem, que irá se conectar com um servidor da sua
operadora, que irá se conectar em outros servidores, tanto da operadora quando
de outras, até chegar no servidor do e-commerce; este vai mandar uma resposta,
que tem que fazer todo o caminho contrário até chegar de volta ao seu
computador.

Com uma CDN como a nossa, em que colocamos servidores diretamente na
operadora, esse processo pode ser reduzido a: seu computador conecta no seu
modem, que conecta no servidor da operador, que conecta no nosso e retorna
imediatamente o resultado, sem precisar fazer todo o caminho até o servidor do
e-commerce. Este servidor é chamado de *origem*.

Isso é uma vantagem para o e-commerce, porque o servidor origem não precisa
responder todos os requests e, portanto, não precisa são grande; é vantagem
para a operadora, porque qualquer tráfego que saia de sua rede e passe por
outra operador é cobrado; e é vantegem para o usuário, porque o tempo de
resposta fica extremamente reduzido.

Para que fique claro daqui pra frente, temos quatro atores nesse sistema:

- *Usuário* é a pessoa fazendo a requisição do seu computador pessoal.
- *CDN* é o serviço que faz o cache de alguns elementos do servidor de origem.
- *Origem* é o servidor de um cliente da CDN que contém o conteúdo original.
- *Cliente* é a pessoa/empresa que contrata o serviço de CDN.

Ainda, é preciso saber que nem todas as requisições são cacheadas pela CDN:
formulários, por exemplo, normalmente não são cacheados por terem pouco
tempo "de vida" e como podem conter dados exclusivos de um usuário, estes
devem ser tratados sempre pela origem ao invés de serem "barrados" pela CDN.

Geração das Mensagens
=====================

No caso da nossa CDN, quando um servidor recebe uma requisição, os dados desta
são guardados em um arquivo de log. Para o exemplo que eu quero discutir aqui,
vou listar 3 informações importantes nesse log:

- *Cache status*: Se o objeto pedido na requisição do usuário está no
  servidor, não precisamos ir até o servidor de origem do cliente; portanto
  temos um "HIT", caso contrário, temos um "MISS".
- *Request length*: Tamanho da requisição (bytes) vinda do usuário.
- *Bytes sent*: Tamanho da resposta (bytes) que o servidor retornou ao
  usuário.

Essas informações são importantes para informar ao cliente quantas requisições
totais o serviço do cliente recebeu (uma requisição é uma linha de log),
quantas não precisaram chegar à origem ("HIT") e quanto de banda o cliente
economizou com o uso da CDN (já que, em caso de "HIT", a origem não recebe o
*request length* da requisição do usuário e nem precisa responder *bytes sent*
de volta para o mesmo).

Primeiros Passos em Processamento Distribuído
=============================================

Se tivessemos apenas um servidor, fazer o cálculo da economia do cliente seria
fácil: é só ir fazendo o cálculo a medida que as linhas de log fossem
aparecendo.

Se tivessemos vários servidores, mas pudéssemos ter os resultados com um dia
de atraso, poderíamos ir separando os arquivos por dia em cada máquina e, no
final de um dia, enviar para um local centralizado e ir processando -- e assim
evitar que atrasos no envio devido à latência de rede pudessem gerar
resultados incorretos.

E se não precisássemos ser 100% corretos, poderíamos até mesmo calcular com
menos tempo e esquecer qualquer problema de latência de transmissão ou dados
que não foram enviados por alguma falha de rede no final do dia.

Entretanto, queremos resultados 100% corretos, agregando dados de vários
lugares diferentes, que podem ter diferenças no tempo de entrega dos seus
dados e em tempo real.

Mas um problema por vez: Precisamos de um lugar centralizador para armazenar
os logs a serem processados.

Uma solução possível seria usar um banco de dados para cada log e, a medida
que eles vão sendo processados, marcar como usados. Ou que funcionasse como um
FIFO. O problema é que essa tabela tenderia a crescer infinitamente, mantendo
dados que não precisamos mais.

Felizmente, existem "banco de dados" feitos justamente para essa finalidade:
Receber registros, permitir a leitura sequencial na medida que vão entrando e,
de tempos em tempos, remover registros antigos. São os chamados "Serviços de
Mensageria".

Serviços de Mensageria
======================

Um serviço de mensageria (ou do inglês "message broker") é composto por três
partes:

- A primeira é o broker em si, a parte responsável por guardar os dados e
  entregar de forma sequencial.
- O *Produtor* é a parte responsável por enviar os dados para o broker;
  message brokers provêem uma API para envio dos dados, mas a geração dos
  dados em si e como vão ser guardados fica por conta do desenvolvedor.
- O *Consumidor* é a parte responsável por retirar e processar os dados do
  broker; assim como o Produtor, o broker tem uma API para recuperar os dados.

.. image:: /images/message-broker.png
   :alt: Estrutura de um serviço de message broker.

A grande vantagem de um Message Broker é que ele permite que mais produtores
ou consumidores conectem-se ao broker para gerar/consumir mensagens: Se há um
aumento da geração de dados, é possível colocar mais Produtores para conseguir
dar vazão da origem para o broker; e se os consumidores não estiverem dando
conta de consumir todas as mensagens que entram no broker, é possível
adicionar mais consumidores.

Os Message Brokers mais famosos são o RabbitMQ, o Kafka e o AWS SQS.

Batch Processing

Intervalo: Programação funcional

Stream Processing

Lambda architecture


.. _CDN: https://pt.wikipedia.org/wiki/Rede_de_fornecimento_de_conte%C3%BAdo
