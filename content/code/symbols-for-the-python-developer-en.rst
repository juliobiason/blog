"Symbols" For the python Developer
##################################

:date: 2018-05-30 11:04
:modified: 2018-05-30 11:04
:tags: python, lisp, symbols, code
:slug: symbols-for-the-python-developer
:lang: en
:summary: Lisp has a "Symbol", which is an indicator of something. Python
          doesn't, but it's functionality can be replicated.

On my small excursions over the Lisp side, one thing that stuck with me were
"symbols".

Symbols are elements in the code that don't store any value, but they "exist".
For example, the following Clojure piece of code:

.. code-block:: clojure

   (apply map vector [[:a :b :c]
                      [:d :e :f]
                      [:g :h :i]])

This is part of the "map" example. It creates a list of lists, each with three
symbols: ``:a``, ``:b``, ``:c`` and so on. They don't have any values, they
simply *are*.

But where does this touches Python?

I recalled what symbols are recently, in a discussion on our code. We had
something like this in one of our modules:

.. code-block:: python

   ACCELERATION = 'acceleration'
   ACCESS_DENIED = 'access_denied'
   DELIVERY = 'delivery'
   REDIRECT = 'redirect'

Anyone working with Django may have seen a lot of those around: We usually
create those constants and then plug them into ``choices``. And that's the
discussion that ensured: "Why were we using these constants when they are
exactly like the strings?"

My first reaction was to answer "to help ensure the code is correct, because
the use of an undefined variable would explode, while an 'undefined' string
would not". But as the discussion kept going, suddenly the context of Symbols
popped back in my head.

Those variables define the value stored in the database. If the user picks the
option ``ACCELERATION``, ``acceleration`` is stored in the database. But the
value in the database is important? If we changed 
``ACCELERATION = 'acceleration'`` to ``ACCELERATION = 'hubba-hubba'`` would it
make any difference for the code? The answer is "no", because instead of
checking if a value is the same as ``acceleration``, it would compare to
``hubba-hubba`` and still work.

(Obviously, you need to use the constant everywhere when you mean
"acceleration", not its value.)

Here is the kicker to understand Symbols in Python: If the "value" itself is
not important, in a way that you can create a constant, change its
value and things will still work (not considering old data, that is), then you
have a Symbol.
