Projects
########

This is an unorganized, unfiltered page of projects, existing or not [#not]_, that I
had.

Ideas
-----

Toe
^^^

Toe is a "modern" implementation of ``.plan`` files (which are read by the
`Finger`_ protocol); the idea is more akin to a "static microblogging
generator" than Finger itself: one could make small drops of whatever they are
working on and generate the resulting pages out of it.

My scenario of use for this tool is to keep my coworkers aware of whatever I'm
doing, and keeping track of whatever I'm doing for historical reasons -- I
keep trying different options on my current project and I'd be nice to have a
place pointing the change itself and its results.

Currently thinking about using Markdown/ReStructuredText for individual
entries, with support for tagging, in a way that one could track all changes
in a single project, instead of the whole.

This project will be written in Haskell, as a way to learn the language.

Unnamed time tracking app
^^^^^^^^^^^^^^^^^^^^^^^^^

The idea is to create a CLI application to track time spent in projects and
tasks, but that will look like `Toggl`_ and `TaskBook`_ -- the first for the
general management and basic look and the second for the abuse of unicode for
better showing stuff.

This project will be written in Rust, using `Diesel`_ for the storage and
`Console`_ for displaying stuff.

In Progress
-----------

None at the moment.

Completed
---------

None that matters.


.. [#not] Alright, I admit: This is mostly a list of projects I had an idea
   but didn't had the time to work on them and I needed a space to gather
   information and resources about working on them.

.. _Finger: https://en.wikipedia.org/wiki/Finger_protocol

.. _Toggl: http://toggl.com/

.. _TaskBook: https://github.com/klauscfhq/taskbook#--taskbook

.. _Console: https://crates.io/crates/console

.. _Diesel: https://diesel.rs/
