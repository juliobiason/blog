In Which My Game Joke Seems Real
################################

:date: 2015-06-17 14:16:00
:modified: 2015-06-17 14:17:00
:tags: games, phobia, guild wars 2, no man's sky, world of warcraft
:slug: in-which-my-game-joke-seems-real
:summary: For ages, I've been joking that I have a virtuo-claustophobia. Now
          it feels real.
:status: draft

I've been an avid MMO player for a long time already. My latest addiction
[#addiction]_ is Guild Wars 2. And every time someone asks if I want to join
them in some dungeon, I refuse with some lame excuse. My current one -- which
I've been using for some time already -- is "I'm virtuo-claustophobic".

.. [#addiction] Just to put some flavour on it. I'm not that addicted as the
   old stories of WoW tell. As right now there is nothing new to do, I've been
   not playing GW2 for about a month already.
