The Day I Almost Dropped Evernote For Keep
##########################################

:date: 2016-07-01 14:52
:tags: evernote, google keep, pdf
:status: draft

After almost a year being an Evernote user, last night I was pretty damn
pissed. Performance with Firefox is abismal, with pearls like:

.. image:: /images/2016/06/evernote.jpg
   :alt: Evernote on Firefox, doing nothing

In other words: Evernote, running on Firefox (the latest version), doing
absolutely nothing, uses 75% CPU. While one can argue a lot about automatic
updates and whatnot, in this age and year making a web app that is not based
on events is absurd (Firefox profiler says it is wasting a huge amount of time
doing DOM paint, which means Evernote is asking the interface to constantly
redraw itself, even when there is nothing changing -- changes should occur
only on events and I pretty much doubt there is an even happening every 100ms
unless someone coded that way, which still counts as absurd).

The problem is happening for awhile now, but yesterday I got really tired of
this situation, to the point that I decided to search for a new note
management tool.

OneNote was instantly ruled out, because past experiences proved that it
manages to be even slower than Evernote.

Google Keep was the last option in my list. First try, I managed to upload a
bunch of pictures (usage graphs for our product, which I keep stored in case I
need to quickly pull the stats) into a single note, which was good. Next step,
I tried to upload an epub from one of the books I bought online, which I keep
stored along with the highlights and review (so I can quickly search for
something I said about it, get the book and find the correct reference); here,
Keep failed because EPUB is not a valid format for it -- because you need to
keep (derp) this kind of file on Drive. This basically means that I need to
run around to two different places to get the full information.

Also, as Evernote, you can use tags to group notes. Keep also manages to add
those when you type "#tag" in the text, but adds the tag *and* keeps the text.
So you end up with a bunch of duplicated information.

And then you have the case of the web clippers. Web clippers are plugins you
add in your browser and it will fetch a web content and send to the server as
a new note. I use this mostly to record interesting thoughts which, again, I
can use later (e.g., I remember someone said something about "TDD" and I can
either check the tag or search for "TDD" and get all content about TDD,
including some page I read long ago but stored as a note). Evernote offers
clippers for all major browsers, while Keep is a Chrome only thing. So not
only I'd have to switch to another product, but I'd have to switch browsers.

Still counting, for some reason, I really dislike the Material design Google
is pushing.  I'm not a fan of the floating "+" and bright colors for a couple
of reasons (which would require another blog post just to explain them all).
While Evernote doesn't have the most awesome web design ever, it is not -- in
my own eyes -- as annoying as material design.

In face of this all, sadly, I'll keep being an Evernote Premium user. But it's
annoying as hell that Evernote does not care about a good web app that manages
to not fuck up a browser.
