A Trifecta dos Tablets
######################

:date: 2016-02-01 09:41
:tags: windows 10, ipad, lenovo, thinkpad, sony, xperia tablet, android
:lang: pt
:slug: the-tablet-trifecta
:status: draft

Semana passada eu comprei um `Lenovo Thinkpad 8`_, o que levou minha contagem
de tablets para 4, passando pelos maiores sistemas de tablets (eu ainda tenho
que achar alguma coisa com Firefox OS ou Tizen, embora eu tenha Maemo na minha
lista), e assim eu decidi colocar minhas opiniões sobre os três.

.. _Lenovo Thinkpad 8: https://en.wikipedia.org/wiki/ThinkPad_8
