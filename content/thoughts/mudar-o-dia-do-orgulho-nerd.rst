Mudemos o Dia do Orgulho Nerd
#############################

:date: 2015-05-25 12:34
:tags: orgulho nerd, star wars, star trek
:slug: mudemos-o-dia-do-orgulho-nerd
:summary: Dia 25 de maio não parece ser um bom dia para "nerds". Entretanto, há outra data para isso.
:status: draft


**Aviso**: Meio fanboismo da minha parte.

Mais uma vez, chega o dia 25 de maio e começam-se a se falar no dia do orgulho
nerd. O dia foi escolhido porque é o dia em que o primeiro Star Wars (Gerra
nas Estrelas) estreiou nos cinemas.

Entretanto, eu nunca consegui ver "Star Wars" como algo "nerd". Sim, ok, é uma
bela "space opera" e quem não gostaria de poder controlar a Força e blá blá
blá... Mas o que é que tem de "nerd" nisso?

Mas, voltemos um passo: O que é um nerd? Basicamente, um nerd é alguém que
dedica muito tempo à apenas uma coisa: Alguém que passa muito tempo estudando
as estrelas é um nerd em astronomia; alguém que passa muito tempo estudando
temperos e temperaturas e como alimentos e sabores se modificam sobre calor e
pressão é um nerd em nerd em culinária; alguém que passa a vida toda estudando
a estrutura de computadores, as intricidades de sistemas operacionais e as
complexidades de linguagens de programação é um nerd em informática. E assim
por diante.

Essa última definição, do "aficcionado por informática", é o que é
costumeiramente aplicado à palavra "nerd". Poucas pessoas pensam em "nerds de
astronomia", "nerds de culinárias" e todas as outras formas de nerdismo que
existem. O motivo é que hackers (no sentido clássico da palavras, o que hoje
seriam "programadores" ao invés de "pessoas que invadem computadores de forma
ilegal para roubar dados) utilizaram o termo "nerd", anteriormente pejorativo,
como uma bandeira de orgulho. Tirando do Dict.org:

   Hackers developed sense 2 [applied to someone who knows what's really important and
   interesting and doesn't care to be distracted by trivial chatter and silly status games]
   in self-defense perhaps ten years later, and some actually wear "Nerd Pride" buttons, only half as a joke.
	
Ou seja: "nerd" é efetivamente uma palavra que surgiu no meio técnico.

Então porque me incomoda tanto que seja no dia do Star Wars? Porque, no meu
ponto, de vista, apesar de tudo que Star Wars é, ele não representa a cultura
nerd.
