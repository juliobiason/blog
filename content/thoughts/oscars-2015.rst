Oscars 2015 by the Buzz
#######################

:date: 2015-02-22 15:37
:tags: oscars, movies

A few months ago, I shared a link with my predictions for the Oscars 2015. But
the only thing I did was to show who I thought it would win but not *why*.
Here I'll try to explain why I pick each one as my pick. And the reason is: All
my picks where only by the buzz they generated, not by their quality. And,
because Hollywood is predictable, my guess is that the Academy will follow
the critics -- or, maybe, the Academy is so predictable that critics picked
exactly what the Academy likes (basically, a self feeding system: the critics
pick the options the Acaemy will go with because everything is so predictable).

And so, without much more ado:

*Best Motion Picture of the Year*: **Selma**
  There is the thing: There is too much flack going around how the Academy is
  all about "white, old men winning everything". This will be their "Shut up,
  we can vote on a movie about a black man starting an equal rights movement".

*Best Performance by an Actor in a Leading Role*: **Bradley Cooper** (American Sniper)
  I'm not completely betting on this, mostly because I tend to pick only one
  Oscar to each movie ('cause, you know "everybody is a winner"). But here is
  the thing: it's not about Badley himself, it's more about giving a prize to
  Clint Eastwood work, even if it is sideways.

*Best Performance by an Actress in a Leading Role*: **Rosamund Pike** (Gone Girl)
  This is one of the movies I watched and yes, Rosamund playing a fanatical,
  revengeful wife really kills it. I don't think people could relate to her but
  that doesn't mean she didn't do a good job. Besides, the movie generated a lot
  of buzz and, thus, it should get an Oscar.

*Best Performance by an Actor in a Supporting Role*: **J.K. Simmons** (Whiplash)
  This is me going with the critics (mostly because I didn't watch it yet).
  There is not a single one that spoke about
  Whiplash without citing Simmons acting (or Miles Teller, but even Teller
  points Simmons acting on it). Because there are so many critics talking about
  it, the Academy will go with it.

*Best Performance by an Actress in a Supporting Role*: Tie.
  Yes, this is the one I'm not sure about it (again, 'cause I watched only 3
  movies in this whole list but only one of those two):

  **Emma Stone** (Birdman): Birdman is getting a lot of buzz and they could
  pick Stone to give Birdman an Oscar... unless they really go with "everybody
  is a winner" and since Birdman will win another Oscar (keep reading), so Stone
  will end without an Oscar this year.

  **Patricia Arquette** (Boyhood): This is the second movie I watched in the
  list. Honestly, I don't think Arquette was interesting, she never picked my
  sympathy the whole movie (maybe 'cause the whole thing is a "life in fast
  forward" and you keep seeing her do the same stupid mistake over and over
  again). But, again, there is a lot of buzz about Boyhood and this will be the
  "everybody is a winner" for them.

*Best Achievement in Directing*: **Richard Linklater** (Boyhood)
  12 years filming. That's an achievement. This is the obvious choice for the
  prize, because that's what the movie is: How do you direct a movie for
  12 freaking years?

*Best Writing, Screenplay Written Directly for the Screen*: **Dan Gilroy** (Nightcrawler)
  My personal guts for this picked nothing, and there is nobody saying "this
  *is* the movie script of this year". So I picked the only my favorite critic
  mentioned.

*Best Writing, Screenplay Based on Material Previously Produced or Published*: **Paul Thomas Anderson** (Inherent Vice)
  Again, no idea at all.

*Best Animated Feature Film of the Year*: **How to Train Your Dragon 2**
  This is the one I don't agree. At all. I didn't like the movie, the plot was
  weak, the pace was all wrong but, for some reason, "How to Train Your Dragon 2"
  got a lot of other prizes and it's hard for the Academy not go with the others.
  But, again, I'm picking it against my own veridict, even being the only
  movie of the list I watched.

*Best Achievement in Cinematography*: **Emmanuel Lubezki** (Birdman)
  This is the one I had to agree with the critics. Although I haven't watched
  the movie, I know all the work it went to make it seem like a single shot
  back to back.

*Best Achievement in Editing*: **Sandra Adair** (Boyhood)
  Again, only because it took 12 years to film everything. How does one make
  a movie from 12 years of filming? That's probably what they will answer.
  "Grand Budapest Hotel" could take this on, but again, it's like the movie
  was made to win this kind of category, so this seems the obvious choice.

*Best Achievement in Production Design*: **Colleen Atwood** (Into the Woods)
  Again, another thought choice between this and "Grand Budapest Hotel", but
  since it's a fantasy movie, probably gives the design team much more
  flexibility than, say, the team in Interstellar, which had to keep somewhat
  faithful with reality. And Maleficent wasn't impressive at all.

*Best Achievement in Makeup and Hairstyling*: **The Grand Budapest Hotel**
  This is where "Grand Budapest Hotel" will get its "everybody is a winner".
  One could argue about the pick is that "Guardians of the Galaxy" is in the
  list and you had at least 4 characters using makeup from head to toe (or,
  head to shoulders), without CGI. But the Academy seems to have a thing against
  movies about super heroes, so they will go with the Hotel.

  ... unless they want to prove that "oh, don't worry, we changed" and give it.
  But, again, for the "we changed" stuff, they will give the big prize to "Selma",
  which is a "point" against "Guardians".

*Best Achievement in Music Written for Motion Pictures, Original Score*: **Jóhann Jóhannsson** (The Theory of Everything)
  Just to be clear, I haven't seen anything about the music of *any* of the
  nominees. So I picked "Theory of Everything" for the "everybody is a winner".

*Best Achievement in Music Written for Motion Pictures, Original Song*: **Shawn Patterson(Everything is Awesome)** (The Lego Movie)
  Just because it's damn sticky song. But, again, the Academy has this thing
  against things target to kids (even if there is this part to the parents),
  so I'm not betting heavily on this.

*Best Achievement in Sound Mixing*: **Interstellar**
  Because Nolan needs his "everybody is a winner" too.

*Best Achievement in Sound Editing*: **Brent Burge, Jason Canovas** (The Hobbit: The Battle of the Five Armies)
  Again, "everybody is a winner".

*Best Achievement in Visual Effects*: **Stephane Ceretti, Nicolas Aithadi, Jonathan Fawkner, Paul Corbould** (Guardians of the Galaxy)
  Mostly because of the CGI (maybe it's time to split this category into a
  "special camera effects" and "CGI").

I'll stay out of the other categories, only because they are not the ones that
generate any buzz.
