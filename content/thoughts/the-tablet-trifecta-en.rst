The Tablet Trifecta
###################

:date: 2016-02-01 09:34
:tags: windows 10, ipad, lenovo, thinkpad, sony, xperia tablet, android
:lang: en
:slug: the-tablet-trifecta
:status: draft

Last week I bought a `Lenovo Thinkpad 8`_, which bought my tablet count to 4
and crossing all major tablet OSes (I still have to find something with
Firefox OS or Tizen, but I have Maemo in my list), so I decided to pour my
opinions online about the three.

iOS
===

My experience with iOS came with two different versions of the iPad: The "New
iPad", a.k.a. iPad 3 and iPad Air 2.

Android
=======

My experience with Android as a tablet OS came with an Sony Xperia Tablet Z.
It was the only HiDPI Android tablet in Brazil and, after being used to read
things on a tablet, something less than retina is terrible.

Thing is, even with the Xperia being only 20 points below the iPad in point
density, its font rendering was atroicious. Result: reading ebooks on the
Xperia was never the pleasant experience I had with the iPad.

Also, even being the only high-end tablet in Brazil, Sony completely refused
to bring all the other accessories, like a damn cover. So I had to buy it from
China.

Windows 10
==========

Windows 10 is the weirdest of the pack, for one single reason: It manages to
have (some) good integration like iOS and have full access to the device like
Android. The problem with that? It's Windows.

Windows always had this idea of keeping "backwards compability" and, due this,
it sometimes appears like a hodgepodge of applications: You have the new
Windows interface (a.k.a. the "Metro" interface, which now is called
"Universal"), and you have a bunch of applications that look like Windows on
desktop (including a floating window when you try to open files) and you have
everything else.

On top of that, the Windows Store is terrible. You can still manually install
any Windows application, but that just adds to the hodgepodge of interfaces
described above.

Now, if Microsoft would, at least, try to bring developers into the
"Universal" app evelopment, and increase the offers in the Windows Store,
Android would be a hard place. But they don't. And, worse of all, Microsoft
seems more interested in releasing things to iOS than Windows, which only
makes things harder: Even **they** seem uninterested in developing for
Windows.

.. _Lenovo Thinkpad 8: https://en.wikipedia.org/wiki/ThinkPad_8

.. [#jailbreak] Ok, you can jailbreak your device, but my experience says it's
   not worth the space it takes on the device.
