Announcing JulioBiason.Net 3.0
##############################

:date: 2015-02-18 09:13
:tags: meta, blog, pelican

Short version: New blog URL, engine and layout.

Long version: For a long time already, I've been thinking about using a static
blog generator. Not that there is anything wrong with dynamic blog engines
(and I'm a long time `WordPress`_ user, without any issues, specially since my
hosting company -- `Dreamhost`_ -- offers easy updates), but... I don't know, I
think it's easy to automate some stuff when all you have are basic files,
with no API to talk to.

So, here it is. A new blog URL, so all old posts are still visible in their
original paths (although this will be a problem in the future when I decide to
launch a 4.0 blog, but that's a problem for the future); a new engine, as
WordPress is not static, so I decided to go with `Pelican`_, simply because I
know Python (I know there is a huge community for `Jekyll`_, but I'm not a Ruby
guy and I don't want to be a Ruby guy); and finally a new layout, as I took
everything I've been playing with `Zurb Foundation`_ and, since I'd
automagically gain a responsive layout, I did just that. And yes, the `theme`_
is my creation -- and that's why there is a bunch of broken stuff. I'll be
fixing them in the future, as I see them -- or someone reports them to me.

PS: There is actually a hidden thing, some `things I don't want to deal
again`_, which could probably crippling me in what to write (hence why the
content was so dull and boring in the last few months). Because static blogs
don't have comments, I may feel fine in finally discussing them.

.. _WordPress: https://wordpress.org/
.. _Dreamhost: http://www.dreamhost.com/
.. _Pelican: http://blog.getpelican.com/
.. _Jekyll: http://jekyllrb.com/
.. _Zurb Foundation: http://foundation.zurb.com/
.. _theme: https://bitbucket.org/juliobiason/pelican-fancy-foundation
.. _things I don't want to deal again: http://juliobiason.net/2008/02/23/why-half-life-2-failed/
